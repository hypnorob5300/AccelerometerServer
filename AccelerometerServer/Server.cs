﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

namespace AccelerometerServer
{
    public class Server
    {
        public int Port;
        public bool HasClient = false;

        private Socket listener;
        private Socket handler;
        private IPEndPoint localEndPoint;
        private string data = null;
        private byte[] bytes;
        private bool _sending = false;

        public Server(int port)
        {
            Port = port;
        }

        public void StartListening()
        {
            // Data buffer for incoming data.  
            bytes = new Byte[1024];

            //Find the wlan0 IpAddress instance, from stack overflow
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress address = ipHostInfo.AddressList[0];

            foreach (NetworkInterface netInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (netInterface.Name == "wlan0")
                {
                    IPInterfaceProperties ipProps = netInterface.GetIPProperties();
                    foreach (UnicastIPAddressInformation addr in ipProps.UnicastAddresses)
                    {
                        if (addr.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            address = addr.Address;
                            break;
                        }
                    }
                    break;
                }
            }

            localEndPoint = new IPEndPoint(address, Port);

            // Create a TCP/IP socket.  
            listener = new Socket(address.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            
            Console.WriteLine("Hosting server at: " + address.AddressFamily + ", ipv4: " + address.MapToIPv4());
            Console.WriteLine("Port: " + Port);
            ClientListen();
        }

        private void ClientListen()
        {
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.  
                while (true)
                {
                    // Program is suspended while waiting for an incoming connection.  
                    handler = listener.Accept();
                    data = null;

                    // An incoming connection needs to be processed.  
                    while (true)
                    {
                        bytes = new byte[1024];
                        int bytesRec = handler.Receive(bytes);
                        data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        if (data.IndexOf("<EOF>") > -1)
                        {
                            HasClient = true;
                            Console.WriteLine("Client connected.");
                            AppDomain.CurrentDomain.ProcessExit += ClientDisconnect;
                            break;
                        }
                    }
                    //handler.Shutdown(SocketShutdown.Both);
                    //handler.Close();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void ClientDisconnect(object sender, EventArgs e)
        {
            HasClient = false;
            //Not sure if i need to re listen but ohh well :/
        }

        public void Send(string data)
        {
            if (!_sending)
            {
                _sending = true;
                byte[] msg = Encoding.ASCII.GetBytes(data);
                try
                {
                    handler.Send(msg);
                }
                catch (SocketException e)
                {
                    Console.WriteLine("Socket exception: " + e.Message + " | Code: " + e.SocketErrorCode);
                    Console.WriteLine("Caught, message probably didnt send.");
                    if (e.SocketErrorCode == SocketError.Shutdown)
                    {
                        HasClient = false;

                        //Re listen
                        ClientListen();
                        
                    }
                }
                _sending = false;
            }
        }
    }
}
