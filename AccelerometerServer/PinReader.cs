﻿using System;
using System.Threading.Tasks;
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Gpio;
using Unosquare.RaspberryIO.Native;

namespace AccelerometerServer
{
    public class PinReader
    {
        public event Action ReadBegin;

        public int Pin { get; }
        public uint ReadValue { get; private set; }
        public uint AccelerationValue { get; private set; }

        private GpioPin pin;

        public PinReader(GpioPin pin)
        {
            this.pin = pin;
            Pin = pin.PinNumber;
            Init();
        }

        public PinReader(int pinNumber)
        {
            Pin = pinNumber;
            pin = Pi.Gpio[Pin];
            Init();
        }

        private void Init()
        {
            ReadValue = 999999999;
            pin.PinMode = GpioPinDriveMode.Input;

            Console.WriteLine("Pin reader has began reading pin (wiring no): " + Pin + ", BCM no: " + pin.BcmPinNumber);
        }

        public uint[] GetValueAsync()
        {
            GpioPinValue val;
            bool haveStart = false;
            bool haveReStart = false;
            bool haveEnd = false;
            bool havePulse = false;

            uint startTime = 0;
            uint endTime = 0;
            uint reStartTime = 0;

            while (true)
            {
                val = pin.ReadValue();
                if (!havePulse)
                {
                    if (!haveStart && val == GpioPinValue.Low)
                    {
                        startTime = Timing.Instance.MicrosecondsSinceSetup;
                    }
                    if (startTime != 0 && !haveStart && val == GpioPinValue.High) haveStart = true;

                    if (haveStart && val == GpioPinValue.High)
                    {
                        endTime = Timing.Instance.MicrosecondsSinceSetup;
                    }
                    if (endTime != 0 && haveStart && !haveEnd && val == GpioPinValue.Low) haveEnd = true;

                    if(haveStart && haveEnd)
                    {
                        havePulse = true;
                    }
                }
                //Check for when high ends, then wait for the next high. (high -> low -> high)
                else if (val == GpioPinValue.High)
                {
                    reStartTime = Timing.Instance.MicrosecondsSinceSetup;
                    haveReStart = true;
                }

                if (haveStart && haveEnd && haveReStart)
                {
                    //if (endTime < startTime) startTime = UInt16.MaxValue - startTime;
                    //if (reStartTime < endTime) reStartTime = UInt16.MaxValue - reStartTime;
                    ReadValue = endTime - startTime;
                    AccelerationValue = ReadValue / reStartTime - startTime;
                    break;
                }
            }

            return new uint[] {ReadValue, AccelerationValue};
        }
    }
}
