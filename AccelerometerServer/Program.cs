﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Gpio;
using Unosquare.RaspberryIO.Native;

namespace AccelerometerServer
{
    class Program
    {
        public static PinReader XOne;
        public static PinReader YOne;
        public static PinReader XTwo;
        public static PinReader YTwo;
        public static Server ServerSocket;

        private static SendData _data;
        private static uint _lastPrintTime = 0;

        static void Main(string[] args)
        {
            Console.WriteLine("Accelerometer Server Start");

            //TestClient.StartClient();
            //Console.ReadKey();
            //return;

            int xonebcm = 17;
            int yonebcm = 18;
            int xtwobcm = 22;
            int ytwobcm = 23;

            if (args.Length > 0)
            {
                args[0] = args[0].ToLower();
                if (args[0] == "--help"|| args[0] == "-help" || args[0] == "help")
                {
                    Console.WriteLine("Argument order for BCM PIN numbers: Acc#1:X Acc#1:Y Acc#2:X Acc#2:Y ");
                    return;
                }
                if (args.Length >= 4)
                {
                    int outt;
                    for (int i = 0; i < 4; i++)
                    {
                        if (!Int32.TryParse(args[i], out outt))
                        {
                            Console.WriteLine("Provided pin numbers did not parse to ints.");
                            return;
                        }
                    }
                    xonebcm = Convert.ToInt32(args[0]);
                    yonebcm = Convert.ToInt32(args[1]);
                    xtwobcm = Convert.ToInt32(args[2]);
                    ytwobcm = Convert.ToInt32(args[3]);
                }
            }

            //Construct and read the pin 1. We use bcm number here to be sure.
            //  NB:(My case and pinout.xyz mark clearly the bcm number vs wireing numbers)


            XOne = new PinReader(Pi.Gpio.GetGpioPinByBcmPinNumber(xonebcm));
            YOne = new PinReader(Pi.Gpio.GetGpioPinByBcmPinNumber(yonebcm));

            XTwo = new PinReader(Pi.Gpio.GetGpioPinByBcmPinNumber(xtwobcm));
            YTwo = new PinReader(Pi.Gpio.GetGpioPinByBcmPinNumber(ytwobcm));

            _data = new SendData();

            ServerSocket = new Server(5555);
            Thread serverThread = new Thread(ServerSocket.StartListening);
            serverThread.Start();

            while (true)
            {
                GetAccelerometerValues();
                ServerUpdate();
                OutputToConsole();
            }
        }

        public static void GetAccelerometerValues()
        {
            uint[] xone = XOne.GetValueAsync();
            uint[] yone = YOne.GetValueAsync();

            _data.One.XRaw = xone[0];
            _data.One.XAcceleration = xone[1];
            _data.One.YRaw = yone[0];
            _data.One.YAcceleration = yone[1];

            uint[] xtwo = XTwo.GetValueAsync();
            uint[] ytwo = YTwo.GetValueAsync();

            _data.Two.XRaw = xtwo[0];
            _data.Two.XAcceleration = xtwo[1];
            _data.Two.YRaw = ytwo[0];
            _data.Two.YAcceleration = ytwo[1];
        }

        public static void ServerUpdate()
        {
            if(ServerSocket.HasClient) ServerSocket.Send(JsonConvert.SerializeObject(_data) + "<EOF>");
        }

        public static void OutputToConsole()
        {
            if (_lastPrintTime + 1000 < Timing.Instance.MillisecondsSinceSetup)
            {
                Console.WriteLine("Accelerometer 1: \nX: " + _data.One.XAcceleration + " raw:(" + _data.One.XRaw + ")\n" +
                                  "Y: " + _data.One.YAcceleration + " raw:(" + _data.One.YRaw + ")");
                Console.WriteLine("Accelerometer 2: \nX: " + _data.Two.XAcceleration + " raw:(" + _data.Two.XRaw + ")\n" +
                                  "Y: " + _data.Two.YAcceleration + " raw:(" + _data.Two.YRaw + ")");
                _lastPrintTime = Timing.Instance.MillisecondsSinceSetup;
            }
        }
    }

    [Serializable]
    public struct SendData
    {
        public Accelerometer One;
        public Accelerometer Two;
    }

    [Serializable]
    public struct Accelerometer
    {
        public uint XRaw;
        public uint XAcceleration;
        public uint YRaw;
        public uint YAcceleration;
    }
}
